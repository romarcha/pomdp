#include <gtest/gtest.h>

#include <fstream>

#include <pomdp/utils/prob_rob_pomdp.h>

using namespace pomdp;

TEST(Pomdp, Reward)
{
    ProbRoboticsPOMDP example = ProbRoboticsPOMDP();

    std::cout<<"Reward action 0: "<<example.reward_belief(0)<<std::endl;
    std::cout<<"Reward action 1: "<<example.reward_belief(1)<<std::endl;
    std::cout<<"Reward action 2: "<<example.reward_belief(2)<<std::endl;
}

TEST(Pomdp, BeliefUpdate)
{
    ProbRoboticsPOMDP example = ProbRoboticsPOMDP();
    for(unsigned int i = 0; i < 10000; i++)
    {
        example.belief_update(2,1);
        example.belief_update(2,0);
    }

    for(unsigned int i = 0; i < example.get_belief().size(); i++)
    {
        std::cout<<example.get_belief()[i]<<std::endl;
    }
}
