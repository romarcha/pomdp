#include <gtest/gtest.h>

#include <pomdp/utils/mdp_example_1.h>

using namespace pomdp;

TEST(Mdp, TransitionMatrixes)
{
    MdpExample1 example = MdpExample1(0.8);
    //Each transition matrix has to sum up to 9.
    //because we have 11 states, where two of them are absorbing.
    //For each non absrobing state the row should add up to one.
    for(unsigned int i = 0 ; i < example.m_trans_matrixes.size(); i ++)
    {
        EXPECT_EQ(example.m_trans_matrixes[0].sum(),9);
    }
}

TEST(Mdp, ValueIteration)
{
    MdpExample1 example = MdpExample1(0.8);
    example.value_iteration();
    //The expected result after value iteration is
    //  V =
    //    75.1586
    //    70.9022
    //    69.2899
    //    -99.9
    //    79.7103
    //    92.8488
    //    99.9
    //    83.5995
    //    87.9548
    //    91.8189
    //    95.5621
    // The value function for each state.
     std::vector<double> V = {
                                75.1586,
                                70.9022,
                                69.2899,
                                -99.9,
                                79.7103,
                                92.8488,
                                99.9,
                                83.5995,
                                87.9548,
                                91.8189,
                                95.5621 };

    double max_error = 0.0001;
    for(unsigned int i = 0; i < example.get_value_function().rows();i++)
    {
        EXPECT_NEAR(example.get_value_function()[i],V[i],max_error);
    }

    example.calculate_policy();
    //The expected policy using the value function provided before is
    //  policy =
    //    1
    //    3
    //    3
    //    0
    //    1
    //    2
    //    0
    //    2
    //    2
    //    2
    //    0
    // The action to be taken at each state.
    std::vector<unsigned int> expected_policy = {
                                    1,
                                    3,
                                    3,
                                    0,
                                    1,
                                    2,
                                    0,
                                    2,
                                    2,
                                    2,
                                    0};

    for(unsigned int i = 0; i < example.get_policy().size();i++)
    {
        EXPECT_EQ(example.get_policy()[i],expected_policy[i]);
    }
}

