#include <pomdp/utils/prob_rob_pomdp.h>

ProbRoboticsPOMDP::ProbRoboticsPOMDP()
{
    State s0(0);
    State s1(1);
    add_state(s0);
    add_state(s1);

    Action a0(0);
    Action a1(1);
    Action a2(2);
    add_action(a0);
    add_action(a1);
    add_action(a2);

    Observation z0(0);
    Observation z1(1);
    add_observation(z0);
    add_observation(z1);

    m_trans_matrix.resize(m_states.size(),m_states.size());
    m_trans_matrix << 0.2, 0.8,
                      0.8, 0.2;

    m_reward_matrix.resize(m_states.size(),m_actions.size());
    m_reward_matrix << -100, 100, -1,
                        100, -50, -1;

    m_obs_matrix.resize(m_states.size(),m_observations.size());
    m_obs_matrix << 0.7, 0.3,
                    0.3, 0.7;

    m_belief.push_back(0.5);
    m_belief.push_back(0.5);

    set_discount_factor(1);
}

ProbRoboticsPOMDP::~ProbRoboticsPOMDP()
{

}

double ProbRoboticsPOMDP::T(unsigned int init_state_idx,
                            unsigned int action_idx,
                            unsigned int final_state_idx)
{
    if(action_idx == 2)
    {
        return m_trans_matrix(init_state_idx,final_state_idx);
    }
    else
        return 0;
}

double ProbRoboticsPOMDP::O(unsigned int state_idx,
                            unsigned int action_idx,
                            unsigned int observation_idx)
{
    return m_obs_matrix(state_idx,observation_idx);
}

double ProbRoboticsPOMDP::R(unsigned int state_idx,
                            unsigned int action_idx)
{
    return m_reward_matrix(state_idx,action_idx);
}
