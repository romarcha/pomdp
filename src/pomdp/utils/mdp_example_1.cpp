#include <pomdp/utils/mdp_example_1.h>

MdpExample1::MdpExample1(double p_)
    :p(p_)
{
    State s1(1);
    State s2(2);
    State s3(3);
    State s4(4);
    State s5(5);
    State s6(6);
    State s7(7);
    State s8(8);
    State s9(9);
    State s10(10);
    State s11(11);

    add_state(s1);
    add_state(s2);
    add_state(s3);
    add_state(s4);
    add_state(s5);
    add_state(s6);
    add_state(s7);
    add_state(s8);
    add_state(s9);
    add_state(s10);
    add_state(s11);

    Action north(0);
    Action south(1);
    Action east(2);
    Action west(3);
    add_action(north);
    add_action(south);
    add_action(east);
    add_action(west);

    Eigen::MatrixXd north_matrix;
    north_matrix.resize(m_states.size(),m_states.size());
    north_matrix <<
                 // 1         2         3         4         5         6         7         8         9        10        11
            p+(1-p)/2,  (1-p)/2,        0,        0,        0,        0,        0,        0,        0,        0,        0,
              (1-p)/2,        p,  (1-p)/2,        0,        0,        0,        0,        0,        0,        0,        0,
                    0,  (1-p)/2,        p,  (1-p)/2,        0,        0,        0,        0,        0,        0,        0,
                    0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,
                    p,        0,        0,        0,    (1-p),        0,        0,        0,        0,        0,        0,
                    0,        0,        p,        0,        0,  (1-p)/2,  (1-p)/2,        0,        0,        0,        0,
                    0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,
                    0,        0,        0,        0,        p,        0,        0,  (1-p)/2,  (1-p)/2,        0,        0,
                    0,        0,        0,        0,        0,        0,        0,  (1-p)/2,        p,  (1-p)/2,        0,
                    0,        0,        0,        0,        0,        p,        0,        0,  (1-p)/2,        0,  (1-p)/2,
                    0,        0,        0,        0,        0,        0,        p,        0,        0,  (1-p)/2,  (1-p)/2;

    Eigen::MatrixXd west_matrix;
    west_matrix.resize(m_states.size(),m_states.size());
    west_matrix <<
            p+(1-p)/2,        0,        0,        0,  (1-p)/2,        0,        0,        0,        0,        0,        0,
                    p,    (1-p),        0,        0,        0,        0,        0,        0,        0,        0,        0,
                    0,        p,  (1-p)/2,        0,        0,  (1-p)/2,        0,        0,        0,        0,        0,
                    0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,
              (1-p)/2,        0,        0,        0,        p,        0,        0,  (1-p)/2,        0,        0,        0,
                    0,        0,  (1-p)/2,        0,        0,        p,        0,        0,        0,  (1-p)/2,        0,
                    0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,
                    0,        0,        0,        0,  (1-p)/2,        0,        0,p+(1-p)/2,        0,        0,        0,
                    0,        0,        0,        0,        0,        0,        0,        p,    (1-p),        0,        0,
                    0,        0,        0,        0,        0,  (1-p)/2,        0,        0,        p,  (1-p)/2,        0,
                    0,        0,        0,        0,        0,        0,  (1-p)/2,        0,        0,        p,  (1-p)/2;

    Eigen::MatrixXd east_matrix;
    east_matrix.resize(m_states.size(),m_states.size());
    east_matrix <<
              (1-p)/2,        p,        0,        0,  (1-p)/2,        0,        0,        0,        0,        0,        0,
                    0,    (1-p),        p,        0,        0,        0,        0,        0,        0,        0,        0,
                    0,        0,  (1-p)/2,        p,        0,  (1-p)/2,        0,        0,        0,        0,        0,
                    0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,
              (1-p)/2,        0,        0,        0,        p,        0,        0,  (1-p)/2,        0,        0,        0,
                    0,        0,  (1-p)/2,        0,        0,        0,        p,        0,        0,  (1-p)/2,        0,
                    0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,
                    0,        0,        0,        0,  (1-p)/2,        0,        0,  (1-p)/2,        p,        0,        0,
                    0,        0,        0,        0,        0,        0,        0,        0,    (1-p),        p,        0,
                    0,        0,        0,        0,        0,  (1-p)/2,        0,        0,        0,  (1-p)/2,        p,
                    0,        0,        0,        0,        0,        0,  (1-p)/2,        0,        0,        0,p+(1-p)/2;

    Eigen::MatrixXd south_matrix;
    south_matrix.resize(m_states.size(),m_states.size());
    south_matrix <<
                 // 1         2         3         4         5         6         7         8         9        10        11
              (1-p)/2,  (1-p)/2,        0,        0,        p,        0,        0,        0,        0,        0,        0,
              (1-p)/2,        p,  (1-p)/2,        0,        0,        0,        0,        0,        0,        0,        0,
                    0,  (1-p)/2,        0,  (1-p)/2,        0,        p,        0,        0,        0,        0,        0,
                    0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,
                    0,        0,        0,        0,    (1-p),        0,        0,        p,        0,        0,        0,
                    0,        0,        0,        0,        0,  (1-p)/2,  (1-p)/2,        0,        0,        p,        0,
                    0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,
                    0,        0,        0,        0,        0,        0,        0,p+(1-p)/2,  (1-p)/2,        0,        0,
                    0,        0,        0,        0,        0,        0,        0,  (1-p)/2,        p,  (1-p)/2,        0,
                    0,        0,        0,        0,        0,        0,        0,        0,  (1-p)/2,        p,  (1-p)/2,
                    0,        0,        0,        0,        0,        0,        0,        0,        0,  (1-p)/2,p+(1-p)/2;

    m_trans_matrixes.push_back(north_matrix);
    m_trans_matrixes.push_back(south_matrix);
    m_trans_matrixes.push_back(east_matrix);
    m_trans_matrixes.push_back(west_matrix);

    double stdRew = -3;
    double posRew = -100;
    double negRew = 100;

    m_reward_matrix.resize(m_states.size(),1);
    m_reward_matrix << stdRew,
                       stdRew,
                       stdRew,
                       posRew,
                       stdRew,
                       stdRew,
                       negRew,
                       stdRew,
                       stdRew,
                       stdRew,
                       stdRew;
}

MdpExample1::~MdpExample1()
{

}

double MdpExample1::T(unsigned int init_state_idx,
                            unsigned int action_idx,
                            unsigned int final_state_idx)
{
    return m_trans_matrixes[action_idx](init_state_idx,final_state_idx);
}

double MdpExample1::R(unsigned int state_idx,
                            unsigned int action_idx)
{
    return m_reward_matrix(state_idx,0);
}
