//Example from the book probabilitic robotics
#ifndef __DETERMINISTIC_POMDP_H__
#define __DETERMINISTIC_POMDP_H__

#include <pomdp/pomdp/pomdp.h>
#include <Eigen/Core>

using namespace pomdp;

/**
 * \brief Partially Observed Markov Decision Process.
 * Deterministic POMDP is a pomdp that has a deterministic
 * transition probability and deterministic observation idx.
 *
 */

class DeterministicPOMDP
        :public Pomdp
{
    public:

        DeterministicPOMDP();

        virtual ~DeterministicPOMDP();

        double T(unsigned int init_state_idx,
                 unsigned int action_idx,
                 unsigned int final_state_idx);

        double O(unsigned int state_idx,
                 unsigned int action_idx,
                 unsigned int observation_idx);

        double R(unsigned int state_idx,
                 unsigned int action_idx);

    private:
        //Transition matrix used by the transition function.
        Eigen::MatrixXd m_trans_matrix;

        //Observation matrix used by the observation function.
        Eigen::MatrixXd m_obs_matrix;

        //Reward matrix
        Eigen::MatrixXd m_reward_matrix;
};

#endif /* __DETERMINISTIC_POMDP_H__ */
