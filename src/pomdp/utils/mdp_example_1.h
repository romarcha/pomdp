#ifndef __MDP_EXAMPLE_1_POMDP_H__
#define __MDP_EXAMPLE_1_POMDP_H__

#include <pomdp/pomdp/mdp.h>
#include <Eigen/Core>

using namespace pomdp;

/**
 * \brief Markov Decision Process.
 * Exapmle of Markov Decision Process
 * It is a typical example described by the following situation:
 *
 * -----------------
 * |   |   |   |+100|
 * -----------------
 * |   | X |   |-100|
 * -----------------
 * |   |   |   |    |
 * -----------------
 *
 * It is a grid world that contains 11 states.
 * Two of them which are terminal with a reward of +100 and -100 each.
 *
 * The available actions are North, South, East or West.
 *
 *     N
 *
 *  W     E
 *
 *     S
 *
 * Definition of states is as follows:
 *
 * ---------------------
 * | S1 | S2 | S3 | S4 |
 * ---------------------
 * | S5 | X  | S6 | S7 |
 * ---------------------
 * | S8 | S9 |S10 |S11 |
 * ---------------------
 *
 * We leave all the transiition probability to depend of a parameter $p$
 * as the probability of being succesfull.
 * This means that if we take accion N, there is a probability p of being
 * succesfull with that action and a probability (1-p)/2 of going W and a
 * probaiblity (1-p)/2 of going E.
 * Given that there are four actions. The transition probability is not only
 * a matrix, but a matrix for each action.
 */

class MdpExample1
        :public Mdp
{
    public:

        /**
         * \brief MdpExample1
         * Constructor that receives the transition succes probability.
         * This means that if we take accion N, there is a probability p of
         * being succesfull with that action and a probability (1-p)/2 of
         * going W and a probaiblity (1-p)/2 of going E.
         * \param p Transition probability.
         */
        MdpExample1(double p = 0.8);

        virtual ~MdpExample1();

        double T(unsigned int init_state_idx,
                 unsigned int action_idx,
                 unsigned int final_state_idx);

        double R(unsigned int state_idx,
                 unsigned int action_idx);

    public:
        //! Action success probability.
        double p;

        //! Transition matrix used by the transition function.
        std::vector<Eigen::MatrixXd> m_trans_matrixes;

        //! Reward matrix
        Eigen::MatrixXd m_reward_matrix;
};

#endif /* __POMDP_POMDP_H__ */
