#include <pomdp/utils/prob_rob_pomdp.h>
#include <pomdp/pomdp/error.hpp>
#include <iostream>
#include <fstream>

using namespace pomdp;

int main(int argc, char *argv[])
{
    if(argc < 2)
    {
        std::cout<<"Usage: ./example_pomdp_pbvi horizon"<<std::endl;
        std::cout<<"with:"<<std::endl;
        std::cout<<"horizon an integer > 0"<<std::endl;
        return 1;
    }
    int horizon = atoi(argv[1]);
    if(horizon < 0)
    {
        throw BaseException("Horizon cannot be negative.");
    }

    //This is a 2D pomdp, where the value function can be
    //represented using as many alpha vectors as we want.
    //Therefore, each alpha vector has two dimensions.
    ProbRoboticsPOMDP example_pomdp;

    std::vector<Pomdp::belief_vector> belief_set;
    //Create 2D belief set, resolution of 0.01 on p1, automatically determines p2.
    for(double p1 = 0; p1 < 1; p1+=0.01)
    {
        Pomdp::belief_vector belief_vec;
        belief_vec.push_back(p1);
        belief_vec.push_back(1-p1);
        belief_set.push_back(belief_vec);
    }

    std::vector<AlphaVector> result;
    example_pomdp.pb_value_iteration(horizon,belief_set,result);

    for(unsigned int i = 0; i < result.size(); i++)
    {
        for(unsigned int j = 0; j < result[i].get_vector().size(); j++)
        {
            std::cout<<result[i].get_vector()[j]<<"\t";
        }
        std::cout << result[i].get_action_id();
        std::cout<<std::endl;
    }

    std::ofstream myfile;
    myfile.open("pbvi_pomdp_alpha_vectors.txt");
    for(unsigned int i = 0; i < result.size(); i++)
    {
        for(unsigned int j = 0; j < result[i].get_vector().size(); j++)
        {
            myfile<<result[i].get_vector()[j]<<"\t";
        }
        myfile << result[i].get_action_id();
        myfile<<std::endl;
    }
    myfile.close();

    std::ofstream policy_file;
    policy_file.open("pbvi_policy.txt");
    for(unsigned int b = 0; b < belief_set.size(); b++)
    {
        policy_file<<belief_set[b][0]<<"\t"
                   <<example_pomdp.policy(result,belief_set[b])<<std::endl;
    }
    myfile.close();

    return 0;
}
