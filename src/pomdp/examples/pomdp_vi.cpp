#include <pomdp/utils/prob_rob_pomdp.h>
#include <iostream>
#include <fstream>
#include <pomdp/pomdp/error.hpp>

using namespace pomdp;

int main(int argc, char *argv[])
{
    if(argc < 2)
    {
        std::cout<<"Usage: ./example_pomdp_vi horizon"<<std::endl;
        std::cout<<"with:"<<std::endl;
        std::cout<<"horizon an integer > 0"<<std::endl;
        return 1;
    }
    int horizon = atoi(argv[1]);
    if(horizon < 0)
    {
        throw BaseException("Horizon cannot be negative.");
    }

    //This is a 2D pomdp, where the value function can be
    //represented using as many alpha vectors as we want.
    //Therefore, each alpha vector has two dimensions.
    ProbRoboticsPOMDP example_pomdp;

    std::vector<AlphaVector> result;
    example_pomdp.value_iteration(horizon,result);

    for(unsigned int i = 0; i < result.size(); i++)
    {
        for(unsigned int j = 0; j < result[i].get_vector().size(); j++)
        {
            std::cout<<result[i].get_vector()[j]<<"\t";
        }
        std::cout<<std::endl;
    }

    std::ofstream myfile;
    myfile.open("pomdp_alpha_vectors.txt");
    for(unsigned int i = 0; i < result.size(); i++)
    {
        for(unsigned int j = 0; j < result[i].get_vector().size(); j++)
        {
            myfile<<result[i].get_vector()[j]<<"\t";
        }
        myfile<<std::endl;
    }
    myfile.close();
    return 0;
}
