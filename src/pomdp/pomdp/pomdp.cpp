#include <pomdp/pomdp/pomdp.h>
#include <boost/multi_array.hpp>
#include <pomdp/pomdp/error.hpp>

namespace pomdp
{

Pomdp::Pomdp()
{

}

Pomdp::~Pomdp()
{

}

double Pomdp::reward_belief(std::vector<double> belief, unsigned int action_idx)
{
    double reward = 0;
    for(unsigned int i = 0; i < belief.size(); i++)
    {
        reward += R(i,action_idx)*belief[i];
    }
    return reward;
}

double Pomdp::reward_belief(unsigned int action_idx)
{
    return reward_belief(m_belief,action_idx);
}

void Pomdp::belief_update(unsigned int action_idx, unsigned int observation_idx)
{
    //Past belief
    std::vector<double> past_belief = m_belief;

    for(unsigned int i = 0; i < m_belief.size(); i++)
    {
        double sum = 0;
        for(unsigned int j = 0; j < m_belief.size(); j++)
        {
            sum += T(j,action_idx,i)*past_belief[j];
        }

        m_belief[i] = O(i,action_idx,observation_idx)*sum;
    }

    //Calculate p(z|b_,a) normalizing constant.
    //First calculated manually, then if it can be avoided this for loop
    //can be removed and the belief can be normalized at the end.
    double p_z_b_a = 0;
    for(unsigned int i = 0; i < m_belief.size(); i++)
    {
        double sum = 0;
        for(unsigned int j = 0; j < m_belief.size(); j++)
        {
            double trans = T(i,action_idx,j);
            double obs = O(j,action_idx,observation_idx);
            sum += trans*obs;
        }
        p_z_b_a += past_belief[i]*sum;
    }

    for(unsigned int i = 0; i < m_belief.size(); i++)
    {
        m_belief[i] = m_belief[i]/p_z_b_a;
    }
}

void Pomdp::pb_value_iteration(unsigned int horizon,
                               std::vector<belief_vector> &belief_set,
                               std::vector<AlphaVector> &gamma)
{
    gamma.clear();

    AlphaVector initial_av(m_states.size());
    gamma.push_back(initial_av);

    //Iterate over time for the horizon
    for(unsigned int t = 0; t < horizon; t++)
    {
        boost::multi_array<double, 4> v(boost::extents[gamma.size()]
                                                      [m_actions.size()]
                                                      [m_observations.size()]
                                                      [m_states.size()]);
        for(unsigned int k = 0; k < gamma.size(); k++)
        {
            for(unsigned int u = 0; u < m_actions.size(); u++)
            {
                for(unsigned int z = 0; z < m_observations.size(); z++)
                {
                    for(unsigned int j = 0; j < m_states.size(); j++)
                    {
                        double sum = 0;
                        for(unsigned int i = 0; i < m_states.size(); i++)
                        {
                            double obs = O(i,u,z);
                            double trans = T(j,u,i);
                            sum += get_discount_factor()*gamma[k].get_vector()[i]*obs*trans;
                        }
                        v[k][u][z][j] = sum;
                    }
                }
            }
        }

        //Step 2, onstruct gamma_a_b
        std::vector<AlphaVector> gamma_a_b;
        for(unsigned int b = 0; b < belief_set.size(); b++)
        {
            for(unsigned int u = 0; u < m_actions.size(); u++)
            {
                AlphaVector new_vector(m_states.size());
                for(unsigned int i = 0; i < m_states.size(); i++)
                {
                    double sum = 0;
                    for(unsigned int z = 0; z < m_observations.size(); z++)
                    {
                        unsigned int max_index = 0;
                        double max_dot_product;
                        for(unsigned int k = 0; k < gamma.size(); k++)
                        {
                            double dot_product = 0;
                            for(unsigned int j = 0; j < m_states.size(); j++)
                            {
                                double v_val = v[k][u][z][j];
                                dot_product += v_val*belief_set[b][j];
                            }
                            if(k == 0 || dot_product > max_dot_product)
                            {
                                max_index = k;
                                max_dot_product = dot_product;
                            }
                        }
                        sum += v[max_index][u][z][i];
                    }
                    new_vector.set_component(i,R(i,u)+sum);
                    new_vector.set_action_id(u);
                }
                add_to_set(gamma_a_b,new_vector);
            }
        }

        //for all actions and belief points find the vector that maximises the
        //dot product and add only that one to the updated set.

        std::vector<AlphaVector> gamma_;
        for(unsigned int b = 0; b < belief_set.size(); b++)
        {
            unsigned int max_idx = 0;
            double max_dot_product;
            for(unsigned int k = 0; k < gamma_a_b.size(); k++)
            {
                double dot_product = 0;
                for(unsigned int i = 0; i < m_states.size(); i++)
                {
                    dot_product += gamma_a_b[k].get_vector()[i]*belief_set[b][i];
                }
                if(k == 0 || dot_product > max_dot_product)
                {
                    max_dot_product = dot_product;
                    max_idx = k;
                }
            }
            add_to_set(gamma_,gamma_a_b[max_idx]);
        }
        gamma = gamma_;
    }

}

void Pomdp::value_iteration(
        unsigned int horizon,
        std::vector<AlphaVector> &gamma)
{
    if(horizon > 3)
    {
        throw BaseException("Prunning is not yet implemented. Using an horizon"
                            " larger than 3 will use too much resources");
    }

    gamma.clear();

    AlphaVector initial_av(m_states.size());
    gamma.push_back(initial_av);
    
    //Iterate over time for the horizon
    for(unsigned int t = 0; t < horizon; t++)
    {
        std::vector<AlphaVector> gamma_;
        boost::multi_array<double, 4> v(boost::extents[gamma.size()]
                                                      [m_actions.size()]
                                                      [m_observations.size()]
                                                      [m_states.size()]);
        for(unsigned int k = 0; k < gamma.size(); k++)
        {
            for(unsigned int u = 0; u < m_actions.size(); u++)
            {
                for(unsigned int z = 0; z < m_observations.size(); z++)
                {
                    for(unsigned int j = 0; j < m_states.size(); j++)
                    {
                        double sum = 0;
                        for(unsigned int i = 0; i < m_states.size(); i++)
                        {
                            double obs = O(i,u,z);
                            double trans = T(j,u,i);
                            sum += get_discount_factor()*gamma[k].get_vector()[i]*obs*trans;
                        }
                        v[k][u][z][j] = sum;
                    }
                }
            }
        }

        // Make the cross sum
        for(unsigned int u = 0; u < m_actions.size(); u++)
        {
            std::vector<Pomdp::index_vector> perm =
                get_index_permutation_array(gamma.size(),m_observations.size());
            for(unsigned int k = 0; k < perm.size(); k++)
            {
                AlphaVector new_vector(m_states.size());
                for(unsigned int i = 0; i < m_states.size(); i++)
                {
                    double sum = 0;
                    for(unsigned int z = 0; z < m_observations.size(); z++)
                    {
                        sum += v[perm[k][z]][u][z][i];
                    }
                    new_vector.set_component(i,
                                          get_discount_factor()*(R(i,u) + sum));
                    new_vector.set_action_id(u);
                }
                gamma_.push_back(new_vector);
            }
        }
        //Optional: prune gamma_
        gamma = gamma_;
    }
}

unsigned int Pomdp::policy(std::vector<AlphaVector> &gamma,
                           belief_vector belief)
{
    unsigned int max_idx = 0;
    double max_dot_product;
    for(unsigned int k = 0; k < gamma.size(); k++)
    {
        double dot_product = 0;
        for(unsigned int i = 0; i < m_states.size(); i++)
        {
            dot_product += gamma[k].get_vector()[i]*belief[i];
        }
        if(k == 0 || dot_product > max_dot_product)
        {
            max_dot_product = dot_product;
            max_idx = k;
        }
    }
    return gamma[max_idx].get_action_id();
}

std::vector<double> Pomdp::get_belief()
{
    return m_belief;
}

void Pomdp::add_observation(Observation observation)
{
    m_observations.push_back(observation);
}

Observation Pomdp::get_observation(unsigned int index)
{
    return m_observations[index];
}

void Pomdp::set_initial_belief(std::vector<double> belief)
{
    m_belief = belief;
}

std::vector<Pomdp::index_vector> Pomdp::get_index_permutation_array(
                        unsigned int gamma_size,
                        unsigned int obs_size)
{

    std::vector<unsigned int> P(obs_size,0);

    std::vector<std::vector<unsigned int> > result;
    result.push_back(P);
    int k;
    do
    { /* do whatever you like with a permutation in p[] */
        k = obs_size-1;
        while (k >= 0)
        {
            P[k]++;
            if (P[k] == gamma_size)
            {
                P[k] = 0;
                k--;
            }
            else
            {
                result.push_back(P);
                break;
            }
        }
    }
    while (k >= 0);

    return result;
}

void Pomdp::add_to_set(std::vector<AlphaVector>& set,
                       AlphaVector new_vector)
{
    bool include = true;
    for(unsigned int i = 0; i < set.size(); i++)
    {
        if(set[i].get_vector() == new_vector.get_vector())
        {
            include = false;
            break;
        }
    }
    if(include)
        set.push_back(new_vector);
}

} /* pomdp */
