#ifndef __POMDP_MDP_H__
#define __POMDP_MDP_H__

#include <vector>

#include <boost/shared_ptr.hpp>

#include <Eigen/Core>
#include <Eigen/Dense>

#include <pomdp/pomdp/actions/action.h>
#include <pomdp/pomdp/states/state.h>

namespace pomdp
{

/**
 * \brief Markov Decision Process.
 *
 */

class Mdp
{
    public:

        Mdp();

        virtual ~Mdp();

        /**
         * \brief State Transition Probability Distribution
         * Calculates the probability of transitioning to state s', given that
         * the agent is in state s and selects action a, for any (s,a,s').
         * T(s,a,s'). The sum of T(s,a,s') wrt s' for all s is 1 because it is
         * a conditional probability distribution. T is time invariant.
         *
         * \param init_state_idx Index of the original state
         * \param action_idx Action executed
         * \param final_state_idx Index of the final state
         */
        virtual double T(unsigned int init_state_idx,
                         unsigned int action_idx,
                         unsigned int final_state_idx) = 0;

        /**
         * \brief Reward Function
         * Assigns a numerical value quantifying the utility of performing
         * action a when in state s.
         *
         * \param state_idx Index of the state
         * \param action_idx Action executed
         */
        virtual double R(unsigned int state_idx,
                         unsigned int action_idx) = 0;

        void add_state(State state);
        State get_state(unsigned int index);

        void add_action(Action action);
        Action get_action(unsigned int index);

        void value_iteration();

        void calculate_policy();

        Eigen::VectorXd get_value_function();

        std::vector<unsigned int> get_policy();

        double get_discount_factor();
        void set_discount_factor(double discount_factor_);

    protected:
        //! Index of the current state within the array of states.
        unsigned int m_current_state;

        //! Discount factor for rewards
        //! Between 0 and 1, ensures future reward remains finite.
        double m_discount_factor;

        //! Array of states
        std::vector<State> m_states;

        //! Array of actions
        std::vector<Action> m_actions;

        //! Value function
        Eigen::VectorXd m_value_function;

        //! Policy
        std::vector<unsigned int> m_policy;
};

} /* pomdp */

#endif /* __POMDP_MDP_H__ */
