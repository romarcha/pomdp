#ifndef __POMDP_ACTION_H__
#define __POMDP_ACTION_H__

#include <pomdp/pomdp/generic/object_with_id.h>

namespace pomdp
{

/**
 * \brief Action is a class that encapsulates all the different types of
 *        actions.
 */

class Action
        :public ObjectWithId
{
    public:

        Action(unsigned int id);

        virtual ~Action();
};

} /* pomdp */

#endif /* __POMDP_ACTION_H__ */
