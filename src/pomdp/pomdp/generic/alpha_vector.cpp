#include <pomdp/pomdp/generic/alpha_vector.h>
#include <pomdp/pomdp/error.hpp>

namespace pomdp
{

AlphaVector::AlphaVector(unsigned int state_dimension)
    :m_state_dimension(state_dimension)
    ,m_vector(std::vector<double>(state_dimension,0.0))
{

}

AlphaVector::~AlphaVector()
{

}

void AlphaVector::set_vector(std::vector<double> vector)
{
    if(vector.size() != m_state_dimension)
    {
        throw DimensionException("Alpha vector is originaly of a different "
                                 " dimensionality");
    }
    m_vector = vector;
}

void AlphaVector::set_component(unsigned int index, double value)
{
    if(index > m_state_dimension)
    {
        throw DimensionException("Trying to add component larger than alpha "
                                 "vector maximum size, defined by the state.");
    }
    m_vector[index] = value;
}

std::vector<double> AlphaVector::get_vector() const
{
    return m_vector;
}

void AlphaVector::set_action_id(unsigned int action_id)
{
    m_action_id = action_id;
}

unsigned int AlphaVector::get_action_id()
{
    return m_action_id;
}

} /* pomdp */
