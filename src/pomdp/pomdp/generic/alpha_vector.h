#ifndef __POMDP_ALPHA_VECTOR_H__
#define __POMDP_ALPHA_VECTOR_H__

#include <vector>

namespace pomdp
{

/**
 * \brief Alpha Vector.
 * Stores the vector itself and an action id.
 */

class AlphaVector
{
    public:
        AlphaVector(unsigned int state_dimension);
        virtual ~AlphaVector();

        void set_vector(std::vector<double> vector);
        void set_component(unsigned int index, double value);
        std::vector<double> get_vector() const;

        void set_action_id(unsigned int action_id);
        unsigned int get_action_id();

    private:
        unsigned int m_state_dimension;

        std::vector<double> m_vector;
        unsigned int m_action_id;

};

} /* pomdp */

#endif /* __POMDP_ALPHA_VECTOR_H__ */
