#include <pomdp/pomdp/generic/object_with_id.h>

namespace pomdp
{

ObjectWithId::ObjectWithId(unsigned int id_)
    :m_id(id_)
{

}

ObjectWithId::~ObjectWithId()
{

}

unsigned int ObjectWithId::get_id()
{
    return m_id;
}

void ObjectWithId::set_id(unsigned int id_)
{
    m_id = id_;
}

} /* pomdp */
