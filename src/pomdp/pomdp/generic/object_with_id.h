#ifndef __POMDP_OJBECT_WITH_ID_H__
#define __POMDP_OJBECT_WITH_ID_H__

namespace pomdp
{

/**
 * \brief Object With Id.
 *
 */

class ObjectWithId
{
    public:

        ObjectWithId(unsigned int id);

        virtual ~ObjectWithId();

        unsigned int get_id();
        void set_id(unsigned int id_);

    private:
        unsigned int m_id;
};

} /* pomdp */

#endif /* __POMDP_OJBECT_WITH_ID_H__ */
