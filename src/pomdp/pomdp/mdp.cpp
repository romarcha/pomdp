#include <pomdp/pomdp/mdp.h>

namespace pomdp
{

Mdp::Mdp()
{

}

Mdp::~Mdp()
{

}

void Mdp::add_state(State state)
{
    m_states.push_back(state);
}

State Mdp::get_state(unsigned int index)
{
    return m_states[index];
}

void Mdp::add_action(Action action)
{
    m_actions.push_back(action);
}

Action Mdp::get_action(unsigned int index)
{
    return m_actions[index];
}

void Mdp::value_iteration()
{
    //Check for the lowest reward.
    double low_reward;
    bool is_set = false;
    for(unsigned int i = 0; i < m_states.size(); i++)
    {
        for(unsigned int j = 0; j < m_actions.size(); j++)
        {
            if(!is_set)
            {
                low_reward = R(i,j);
                is_set = true;
            }
            else if(R(i,j) < low_reward)
            {
                low_reward = R(i,j);
            }
        }
    }

    m_value_function.setOnes(m_states.size());
    m_value_function = low_reward*m_value_function;

    double tolerance = 0.01;
    double discount_factor = 0.999;

    bool first_evaluation = true;
    Eigen::VectorXd old_value = m_value_function;

    while(first_evaluation ||
          abs((m_value_function-old_value).maxCoeff()) > tolerance)
    {
        old_value = m_value_function;
        for(unsigned int i = 0; i < m_states.size(); i++)
        {
            //Evaluate for different actions
            Eigen::VectorXd a_values(m_actions.size());
            for(unsigned int a = 0; a < m_actions.size(); a++)
            {
                double sum = 0;
                for(unsigned int j = 0; j < m_states.size(); j++)
                {
                    double trans = T(i,a,j);
                    sum += old_value[j]*trans;
                }
                a_values[a] = R(i,a)+sum;
            }

            double max_a_values = a_values.maxCoeff();
            m_value_function[i] = discount_factor*max_a_values;
        }
        first_evaluation = false;
    }
}

void Mdp::calculate_policy()
{
    //Using the values from the value function, we can calculate a policy.
    for(unsigned int i = 0; i < m_states.size(); i++)
    {
        bool first_evaluated = false;
        double max_act_value;
        unsigned int max_value_index;
        Eigen::VectorXd act_values(m_actions.size());
        for(unsigned int a = 0; a < m_actions.size(); a++)
        {
            //Calculate sum of rewards depending on the transition probability
            double sum = 0;
            for(unsigned int j = 0; j < m_states.size(); j++)
            {
                sum += m_value_function[j]*T(i,a,j);
            }
            act_values[a] = R(i,a)+sum;
            if(!first_evaluated)
            {
                max_act_value = act_values[a];
                max_value_index = a;
                first_evaluated = true;
            }
            else if(act_values[a] > max_act_value)
            {
                max_value_index = a;
                max_act_value = act_values[a];
            }
        }
        m_policy.push_back(max_value_index);
    }
}

Eigen::VectorXd Mdp::get_value_function()
{
    return m_value_function;
}

std::vector<unsigned int> Mdp::get_policy()
{
    return m_policy;
}

double Mdp::get_discount_factor()
{
    return m_discount_factor;
}

void Mdp::set_discount_factor(double discount_factor_)
{
    m_discount_factor = discount_factor_;
}


} /* pomdp */
