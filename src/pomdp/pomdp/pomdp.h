#ifndef __POMDP_POMDP_H__
#define __POMDP_POMDP_H__

#include <pomdp/pomdp/mdp.h>
#include <pomdp/pomdp/observations/observation.h>
#include <pomdp/pomdp/generic/alpha_vector.h>

namespace pomdp
{

/**
 * \brief Partially Observed Markov Decision Process.
 *
 */

class Pomdp
        :public Mdp
{
    public:
        typedef std::vector<unsigned int> index_vector;
        typedef std::vector<double> belief_vector;

        Pomdp();

        virtual ~Pomdp();

        /**
         * \brief Observation Probability Distribution
         * Calculates the probability that the agent will percieve observetaion
         * z upon executing action a in state s.
         * O(s,a,z). The sum of O(s,a,z) wrt z for all s,a is 1 because it is
         * a conditional probability distribution. O is time invariant.
         *
         * \param state_idx Index of the state
         * \param action_idx Action executed
         * \param observation_idx Index of the observation
         */
        virtual double O(unsigned int state_idx,
                         unsigned int action_idx,
                         unsigned int observation_idx) = 0;

        /**
         * \brief Reward Function
         * Assigns a numerical value quantifying the utility of performing
         * action a when the belief is b. It uses the reward function defined
         * for each state (in the MDP) class.
         *
         * \param belief Probability distribution over states
         * \param action_idx Action executed
         */
        double reward_belief(std::vector<double> belief,
                             unsigned int action_idx);

        /**
         * \brief Reward Function
         * Assigns a numerical value quantifying the utility of performing
         * action a when the belief is m_belief.
         * It uses the reward function defined for each state (in the MDP) class.
         *
         * \param action_idx Action executed
         */
        double reward_belief(unsigned int action_idx);

        /**
         * \brief Belief update
         * Everytime a pair action observation is executed the belief is updated.
         *
         * \param action_idx Action executed
         * \param observation_idx Observation
         */
        void belief_update(unsigned int action_idx,
                           unsigned int observation_idx);

        void pb_value_iteration(unsigned int horizon,
                                std::vector<belief_vector> &belief_set,
                                std::vector<AlphaVector> &gamma);

        void value_iteration(unsigned int horizon,
                             std::vector<AlphaVector> &result);

        unsigned int policy(std::vector<AlphaVector> &gamma,
                            belief_vector belief);

        std::vector<double> get_belief();

        void add_observation(Observation observation);
        Observation get_observation(unsigned int index);

        void set_initial_belief(std::vector<double> belief);

    protected:
        //! Array of observations
        std::vector<Observation> m_observations;

        //! Belief Distribution
        //! Probability of each state.
        belief_vector m_belief;

    private:
        std::vector<Pomdp::index_vector> get_index_permutation_array(
                                unsigned int gamma_size,
                                unsigned int obs_size);

        void add_to_set(std::vector<AlphaVector> &set,
                        AlphaVector new_vector);
};

} /* pomdp */

#endif /* __POMDP_POMDP_H__ */
