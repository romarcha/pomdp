#ifndef __POMDP_OBSERVATION_H__
#define __POMDP_OBSERVATION_H__

#include <pomdp/pomdp/generic/object_with_id.h>

namespace pomdp
{

/**
 * \brief Observations is a class that encapsulates all the different
 *        type of observations.
 *
 */

class Observation
        :public ObjectWithId
{
    public:

        Observation(unsigned int id);

        virtual ~Observation();
};

} /* pomdp */

#endif /* __POMDP_OBSERVATION_H__ */
