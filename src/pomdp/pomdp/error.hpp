#ifndef __POMDP_ERROR_HPP__
#define __POMDP_ERROR_HPP__

#include <iostream>
#include <exception>

namespace pomdp
{
    
/**
 * \brief Base class of the exception hierarchy.
 */
class BaseException
    :public std::exception
{
    public:
        /**
        * \brief Creates a new BaseError instance.
        *
        * \param err the error message
        */
        BaseException(std::string const& err)
        :   m_error(err)
        {}

        virtual const char* what() const throw()
        {
            return m_error.c_str();
        }

    private:
        //! Error string of this exception
        std::string                     m_error;
};

/**
 * \brief Error thrown in relation with the dimension of a vector.
 */
class DimensionException
        : public BaseException
{
    public:
        /**
         * \brief Creates a new DimensionError instance.
         *
         * \param err the error message
         */
        DimensionException(std::string const& err)
            :BaseException(err)
        {}
};

} /* pomdp */


#endif /* __POMDP_ERROR_HPP__ */
