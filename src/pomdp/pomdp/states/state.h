#ifndef __POMDP_STATE_H__
#define __POMDP_STATE_H__

#include <pomdp/pomdp/generic/object_with_id.h>

namespace pomdp
{

/**
 * \brief State is a class that encapsulates all the different type of
 *        states.
 *
 */

class State
        :public ObjectWithId
{
    public:

        State(unsigned int id);

        virtual ~State();
};

} /* pomdp */

#endif /* __POMDP_STATE_H__ */
