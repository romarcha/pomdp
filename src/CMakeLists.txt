cmake_minimum_required( VERSION 2.8 )

# +-----------------------------------------------------------------------------
# | Basic settings
# +-----------------------------------------------------------------------------

project( "Pomdp" )

# Set output directories for libraries and executables
set( BASE_DIR ${CMAKE_SOURCE_DIR}/.. )
set( CMAKE_LIBRARY_OUTPUT_DIRECTORY ${BASE_DIR}/lib )
set( CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${BASE_DIR}/lib )
set( CMAKE_RUNTIME_OUTPUT_DIRECTORY ${BASE_DIR}/bin )

# Cmake module search locations
list( APPEND CMAKE_MODULE_PATH ${BASE_DIR}/cmake )

# Prefix to be used for all library names
set( LIB_PREFIX "pomdp_" )

set( BUILD_EXAMPLES true CACHE BOOL "Build the examples?" )
set( BUILD_TESTS true CACHE BOOL "Build the unit tests?" )
set( BUILD_APPS false CACHE BOOL "Build applications?" )
set( GENERATE_CTAGS false CACHE BOOL "Generate ctags file?" )

# +-----------------------------------------------------------------------------
# | Library search and setup
# +-----------------------------------------------------------------------------

# Build shared libraries by default
set( BUILD_SHARED_LIBS True CACHE BOOL "Build shared (dynamic) libraries?" )

# Allow GTest based tests
enable_testing()

# Search for required libraries
find_package( Doxygen )
find_package( GTest )
find_package( Eigen )

# Search for Boost libraries with minimum version
set(BOOST_MIN_VERSION "1.46.0")
find_package( Boost ${BOOST_MIN_VERSION} COMPONENTS system)
if (NOT Boost_FOUND)
    message(FATAL_ERROR "Fatal error: Boost (version >= ${BOOST_MIN_VERSION}) required.\n")
endif (NOT Boost_FOUND)

#Show header files in IDE
file(GLOB_RECURSE library_header_files ${BASE_DIR}/src/pomdp/
*.h *.hpp)
add_custom_target( library_headers SOURCES ${library_header_files} )

include_directories(
    ${BASE_DIR}/src
    ${BASE_DIR}/src/third_party/gtest
    ${EIGEN_INCLUDE_DIRS}
)
link_directories( ${BASE_DIR}/lib )

# +-----------------------------------------------------------------------------
# | Custom targets and macros
# +-----------------------------------------------------------------------------

# Custom target that builds ctags for the source code
if( GENERATE_CTAGS )
    add_custom_target(
        generate_ctags ALL
        ctags -R --c++-kinds=+p --fields=+iaS --extra=+q .
        WORKING_DIRECTORY ${BASE_DIR}/src
        COMMENT "Regenerating ctags file"
    )
endif()

# Generates library name with the correct prefix
# _origname plain name of the library
# _newname  variable to store the new name in
macro( LIBNAME _origname _newname )
    set( ${_newname} "${LIB_PREFIX}${_origname}" )
endmacro()

# Generates API documentation using doxygen
if( DOXYGEN_FOUND )
    add_custom_target(
        doc
        ${DOXYGEN_EXECUTABLE}
        ${BASE_DIR}/doc/documentation.dox
        WORKING_DIRECTORY ${BASE_DIR}/doc
        COMMENT "Generating API documentation with Doxygen"
        VERBATIM
    )
endif()

# +-----------------------------------------------------------------------------
# | Compiler settings
# +-----------------------------------------------------------------------------

if( CMAKE_COMPILER_IS_GNUCXX )
    set( CMAKE_CXX_FLAGS "-std=c++0x" )
endif()


# +-----------------------------------------------------------------------------
# | Compile code
# +-----------------------------------------------------------------------------

# Generate library names
LIBNAME( "pomdp"                   LIB_POMDP            )
LIBNAME( "utils"                   LIB_UTILS            )

# Add subfolders
add_subdirectory( pomdp/pomdp )
add_subdirectory( pomdp/utils )
if( BUILD_EXAMPLES )
    add_subdirectory( pomdp/examples )
endif()
if( BUILD_TESTS )
    add_subdirectory( pomdp/tests )
endif()
if( BUILD_APPS )
    add_subdirectory( apps )
endif()

